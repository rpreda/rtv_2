#include "RT.h"


/*
	Steps for rendering a basic image
	1. Define world space
	2. Create rays from the camera through the screen canvas
	3. Check for intersections betweent the rays and defined objects
	4. PROFIT????
*/





void putPixel(Color col, SDL_Renderer* rend, int x, int y) {
	SDL_SetRenderDrawColor(rend, 255, 255, 255, 0);
	SDL_RenderDrawPoint(rend, x, y);
}

void render(SDL_Renderer* rend) {
	std::cout << "Starting render!"<<std::endl;

	//Quick and dirty "La impresie" shape definition
	const Vec3d center = Vec3d(0.8f, 0.2f, 2);//temp sphere center
	const float radius = 1;


	const float pix_x_err = screen_size_x / SCREEN_WIDTH / 2;
	const float pix_y_err = screen_size_y / SCREEN_HEIGHT / 2;
	//Iterate over the pixels and generate the proper pixel center point for tracing the ray from camera thru canvas
	//TODO: Flip for proper orientation
	for (int i = 0; i < SCREEN_WIDTH; i++) {
		for (int j = 0; j < SCREEN_HEIGHT; j++) {
			//Generate pixel vector (center point of a square pixel)
			Vec3d pix = Vec3d(screen_size_x / SCREEN_WIDTH * i + pix_x_err,
				//the subtraction solves the vertical flip probelm due to the way the space is implemented
							  screen_size_y / SCREEN_HEIGHT * (SCREEN_HEIGHT - j) + pix_y_err,
							  0);
			Vec3d v = pix - camera;//ray direction
			 
			//QUICK and dirty temporary render of a sphere
			Vec3d f = Vec3d(camera.x - center.x, camera.y - center.y, camera.z - center.z);

			float a = v.x * v.x + v.y * v.y + v.z * v.z;
			float b = 2 * (v.x * f.x + v.y * f.y + v.z * f.z);
			float c = f.x * f.x + f.y * f.y + f.z * f.z - radius * radius;
			float x1, x2;
			if (solveQadratic(a, b, c, &x1, &x2))
				putPixel(Color(), rend, i, j);

		}
	}
}