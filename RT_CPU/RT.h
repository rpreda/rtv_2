#pragma once

#include <iostream>
#include <SDL.h>
#include "Utils.h"


//Parameters 2D
constexpr auto SCREEN_WIDTH = 1280;
constexpr auto SCREEN_HEIGHT = 720;
constexpr auto RENDER_CORE_COUNT = 8;


//WORLD structure ONLY 1920/1080 aspect ratio
const Vec3d camera = Vec3d(0.8888888f, 0.5f, -1);
const Vec3d screen_low_left = Vec3d(0, 0, 0);
const Vec3d screen_top_left = Vec3d(0, 1, 0);
const Vec3d screen_low_right = Vec3d(1.7777777f, 0, 0);
const Vec3d screen_top_right = Vec3d(1.7777777f, 1, 0);
const float screen_size_x = 1.77777777777f;
const float screen_size_y = 1;

//Functions

void render(SDL_Renderer* renderer);

/*

Screen coordinates:
(0,1,0)	->	/-------------------\  <- (1.7777, 1, 0)
			|                   |
			|                   |
			|                   |
(0,0,0)	->	\-------------------/  <- (1.7777, 0, 0)

Camera coordinates:
Behind (negative z) the screen in the center -> (0.888888, 0.5, -1)

*/