#pragma once
#include <cmath>
#include <iostream>

class Color {
public:
	float r;
	float g;
	float b;
	void sanityCheck() {
		if (r > 1) r = 1;
		if (g > 1) g = 1;
		if (b > 1) b = 1;
	}
	unsigned int rInt() {
		sanityCheck();
		return (int)abs(255 * r);
	}
	unsigned int gInt() {
		sanityCheck();
		return (int)abs(255 * g);
	}
	unsigned int bInt() {
		sanityCheck();
		return (int)abs(255 * b);
	}
	Color(float _r, float _g, float _b) {
		r = _r;
		g = _g;
		b = _b;
	}
	Color() {
		r = 1;
		g = 1;
		b = 1;
	}
};

class Vec3d {
public:
	float x = 0, y = 0, z = 0;
	Vec3d(float _x, float _y, float _z);
	Vec3d operator+(const Vec3d& v);
	Vec3d operator-(const Vec3d& v);
	float dotProduct(Vec3d other);
	Vec3d crossProduct(Vec3d other);
	float magnitude();
	float angle(Vec3d);
	void print();
};

//Mathematics utils 
bool solveQadratic(float a, float b, float c, float* x1, float* x2);