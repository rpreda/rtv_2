#include "Utils.h"
//Vec3d

Vec3d::Vec3d(float _x, float _y, float _z) {
	x = _x;
	y = _y;
	z = _z;
}
Vec3d Vec3d::operator+(const Vec3d& v) {
	return Vec3d(v.x + this->x, v.y + this->y, v.z + this->z);
}

Vec3d Vec3d::operator-(const Vec3d& v) {
	return Vec3d(this->x - v.x, this->y - v.y, this->z - v.z);
}

Vec3d Vec3d::crossProduct(Vec3d other) {
	return Vec3d(this->y * other.z - this->z * other.y,
		this->z * other.x - this->x * other.z,
		this->x * other.y - this->y * other.y);
}

float Vec3d::dotProduct(Vec3d other) {
	return this->x * other.x + this->y * other.y + this->z * other.z;
}

float Vec3d::magnitude() {
	return sqrtf(x*x + y*y + z*z);
}

float Vec3d::angle(Vec3d other) {
	return this->dotProduct(other) / (other.magnitude() * this->magnitude());
}

void Vec3d::print() {
	std::cout << x << " " << y << " " << z << std::endl;
}

//Other utils

bool solveQadratic(float a, float b, float c, float *x1, float *x2) {
	float delta = b * b - 4 * a * c;
	if (delta < 0)//no real solution
		return false;
	float delta_sq = sqrtf(delta);
	*x1 = (-b + delta_sq) / (2 * a);
	*x2 = (-b - delta_sq) / (2 * a);
	return true;
}